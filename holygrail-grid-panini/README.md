# HolyGrail

A theme with the Holy Grail **Grid CSS** Layout.

- Why is `header, aside, main, footer` used?
  - HTML5 elements do not require `ariia` labels, it's built in.

## Setup

First, install the local packages:

```sh
npm i
# or
yarn
```

## Transpile with Gulp 4

To transpile, install gulp 4.X globally. I also added `ls-scripts`
is optional, but it's very useful to see what commands you can run in
any given `package.json.`

```sh
npm i -g gulp@latest gulp-cli@latest ls-scripts
```

I like to add this to `package.json`, which is already in this project.

```json
"scripts": {
  "start": "gulp",
  "dev": "yarn start",
  "build": "gulp build"
}
```

### Usage: Dev Server

Run the Dev Server with any of these:

```sh
gulp
# or
yarn start
yarn dev  # Just an Alias
```

### Usage: Build

Run the final build with any of these:

```sh
gulp build
# or
yarn build
```

---

License: MIT _(Open Source)_

Copyright 2019 Jesse Boyer <<https://jream.com>>
